/*
 * WS2812P16 example
 *
 * Created: 2020.04.05 
 * Author: lucabuka
 * 
 * This version of the lib (P16) use 4-bits for each Pixel.
 * 
 * This example make the LED at pos 0 'blinks'
 * with the colors defined in the Palette
 * On startup, turn off every LED in the Strip
 * 
 */ 

#include <WS2812P16.h>


#define NLED 300      // Set this according to your strip lenght
#define OUTPUTL_PIN 2 // Digital output pin (default: 2) 


WS2812P16 LED(NLED); 

// User-defined Palette
// Change this values according to the color set
// you need in your app.
uint8_t palette[][3]={ 
         //  G    R    B       Idx
           {000, 000, 000}, //  0 - Black
           {000, 128, 000}, //  1 - Red
           {128, 000, 000}, //  2 - Green
           {000, 000, 128}, //  3 - Blue
           {128, 128, 128}, //  4 - White
           {255, 255, 000}, //  5 - Yellow
           {000, 255, 255}, //  6 - Cyan
           {  8,   0,   8}, //  7 - Magenta
           { 96,  36,  96}  //  8 - Orchid 
        };
uint8_t palette_size = sizeof(palette) / 3;

void setup() {
	LED.setOutput(2); // Digital Pin 2
  LED.setPalette(&palette[0][0]); // Pointer to the user-defined palette above
  
  // Turn off LEDs
  for(int i=0; i<NLED;i++){
    LED.setPixelColorId(i, 0); // Palette[0] = BLACK
  }
  LED.sync();
}

void loop() {
  for(uint8_t i=0; i<palette_size;i++) { 
    LED.setPixelColorId(0, i);
  	LED.sync(); // Sends the value to the LED
	  delay(500); // Wait 500 ms
  }
}
