/*
 * WS2812P16 example
 *
 * Created: 2020.04.05 
 * Author: lucabuka
 * 
 * This version of the lib (P16) use 4-bits for each Pixel.
 * 
 * This example turn on ramdoms LED in the
 * strip with different palette colors
 * On startup, turn off every LED in the Strip
 * 
 */ 

#include <WS2812P16.h>

#define NLED 300      // Set this according to your strip lenght
#define OUTPUTL_PIN 2 // Digital output pin (default: 2) 


WS2812P16 LED(NLED); 

// User-defined Palette
// Change this values according to the color set
// you need in your app.
uint8_t palette[][3]={ 
         //  G    R    B       Idx
           {000, 000, 000}, //  0 - Black
           {000, 128, 000}, //  1 - Red
           {128, 000, 000}, //  2 - Green
           {000, 000, 128}, //  3 - Blue
           {128, 128, 128}, //  4 - White
           {255, 255, 000}, //  5 - Yellow
           {000, 255, 255}, //  6 - Cyan
           {  8,   0,   8}, //  7 - Magenta
           { 96,  36,  96}  //  8 - Orchid 
        };
uint8_t palette_size = sizeof(palette) / 3;

void setup() {
	LED.setOutput(OUTPUTL_PIN); 
  LED.setPalette(&palette[0][0]); // Pointer to the user-defined palette above

  // Turn off LEDs
  for(int i=0; i<NLED;i++){
    LED.setPixelColorId(i, 0); // Palette[0] = BLACK
  }
  LED.sync(); 
  randomSeed(analogRead(0));
}


uint8_t j=0;
void loop() {

  uint8_t i = random(0,NLED+1);
    
  LED.setPixelColorId(i, j);
  LED.sync(); 
  delay(100); 
  LED.setPixelColorId(i, 0); // Palette[0] = BLACK
  j++;
  if(j >= palette_size) j=0;

}
