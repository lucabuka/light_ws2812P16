light_ws2812_P16  -   V1.0
--------------


WS2812P16 lib v1.0 - 2020-04-06 
Author: Gitlab @lucabuka

* Modified version of **light weight WS2812 lib V2.1 by  Tim (cpldcpu@gmail.com)**

  The original lib was modified to use a user-defined Palette.
  The 'Color value' for each Pixel (LED) is **stored in ONE NIBBLE (4-bit)** while the original library uses 3 Bytes.  

	!!! *There is a **"WS2812P"** version of this lib using ONE BYTE per Pixel.* !!!
	https://gitlab.com/lucabuka/light_ws2812p
   
The current implementation of the **WS2812P16** here is considerably **SLOWER than the WS2812P** in sending the data to the LED Strip.
   Use this "P16" version only if you don't have enough RAM and a slow 'refresh' is not a problem.


This is the original Tim's lib" 
- https://github.com/cpldcpu/light_ws2812

