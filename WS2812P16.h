/*
* -- WS2812P16 lib v1.0 - 2020-04-06 
* -- Author: Gitlab @lucabuka
* --     Modified version of "light weight WS2812 lib V2.1" by  Tim (cpldcpu@gmail.com)
* --
* --  The original lib was modified to use a user-defined Palette 
* --  The 'Color value' for each Pixel (LED) is stored in ONE NIBBLE (4-bit) 
* --  while the original library uses 3 Bytes.  
* -- 
* -- PLEASE NOTE !!!
* --    There is a "WS2812P" version of this lib using ONE BYTE per Pixel.
* --    The **WS2812P16** here is considerably **SLOWER than the WS2812P** 
* --    in sending the data to the LED Strip.
* --    Use the "P16" version only if you don't have enough RAM and 
* --    a slow 'refresh' is not a problem.
* -- 
*
* light weight WS2812 lib V2.1 - Arduino support
*
* Controls WS2811/WS2812/WS2812B RGB-LEDs
* Author: Matthias Riegler
*
* Mar 07 2014: Added Arduino and C++ Library
*
* September 6, 2014      Added option to switch between most popular color orders
*                          (RGB, GRB, and BRG) --  Windell H. Oskay
* 
* January 24, 2015       Added option to make color orders static again
*                        Moved cRGB to a header so it is easier to replace / expand 
*                              (added SetHSV based on code from kasperkamperman.com)
*                                              -- Freezy
*
* License: GNU GPL v2 (see License.txt)
*/

#ifndef WS2812P16_H_
#define WS2812P16_H_

#include <avr/interrupt.h>
#include <avr/io.h>
#ifndef F_CPU
#define  F_CPU 16000000UL
#endif
#include <util/delay.h>
#include <stdint.h>

#ifdef ARDUINO
#if (ARDUINO >= 100)
#include <Arduino.h>
#else
#include <WProgram.h>
#include <pins_arduino.h>
#endif
#endif


// CHANGE YOUR STATIC RGB ORDER HERE
#define OFFSET_R(r) r+1
#define OFFSET_G(g) g	
#define OFFSET_B(b) b+2	


class WS2812P16 {
public: 
	WS2812P16(uint16_t num_led);
	~WS2812P16();

	#ifndef ARDUINO
	void setOutput(const volatile uint8_t* port, volatile uint8_t* reg, uint8_t pin);
	#else
	void setOutput(uint8_t pin);
	#endif

	void setPalette(uint8_t *);	
    uint8_t setPixelColorId(uint16_t index, uint8_t pId);
    uint8_t getPixelColorId(uint16_t index);
	void sync();

	
private:
	uint16_t count_led;
	uint8_t *pixels;
	uint8_t *palette;

	void ws2812_sendarray_mask(uint8_t *array,uint16_t length, uint8_t pinmask,uint8_t *port, uint8_t *portreg);

	const volatile uint8_t *ws2812_port;
	volatile uint8_t *ws2812_port_reg;
	uint8_t pinMask; 
};



#endif /* WS2812P16_H_ */
