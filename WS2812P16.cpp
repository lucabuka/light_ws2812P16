/*
* -- WS2812P16 lib v1.0 - 2020-04-06 
* -- Author: Gitlab @lucabuka
* --     Modified version of "light weight WS2812 lib V2.1" by  Tim (cpldcpu@gmail.com)
* --
* --  The original lib was modified to use a user-defined Palette 
* --  The 'Color value' for each Pixel (LED) is stored in ONE NIBBLE (4-bit) 
* --  while the original library uses 3 Bytes.  
* -- 
* -- PLEASE NOTE !!!
* --    There is a "WS2812P" version of this lib using ONE BYTE per Pixel.
* --    The **WS2812P16** here is considerably **SLOWER than the WS2812P** 
* --    in sending the data to the LED Strip.
* --    Use the "P16" version only if you don't have enough RAM and 
* --    a slow 'refresh' is not a problem.
* -- 
*
* light weight WS2812 lib V2.1 - Arduino support
*
* Controls WS2811/WS2812/WS2812B RGB-LEDs
* Author: Matthias Riegler
*
* Mar 07 2014: Added Arduino and C++ Library
*
* September 6, 2014:	Added option to switch between most popular color orders
*						(RGB, GRB, and BRG) --  Windell H. Oskay
* 
* License: GNU GPL v2 (see License.txt)
*/

#include "WS2812P16.h"
#include <stdlib.h>

WS2812P16::WS2812P16(uint16_t num_leds) {
	count_led = num_leds;
	uint16_t arraySize = (num_leds / 2) + (num_leds % 2);
	pixels = (uint8_t*)malloc(arraySize);
}


void WS2812P16::setPalette(uint8_t *palette_ptr) {
	palette = palette_ptr;
}


uint8_t WS2812P16::setPixelColorId(uint16_t index, uint8_t pId) {
	
	if(index < count_led) {
        uint16_t byteIDx = index >> 1; // Index / 2
        if(index%2 == 0) {    // index is an even number -> Nibble[0]
            pixels[byteIDx] = (pixels[byteIDx] & 0xF0) | (pId & 0xF);
        } else {   // index is an odd number -> Nibble[1]
            pixels[byteIDx] = (pixels[byteIDx] & 0x0F) | ((pId & 0xF) << 4);
        }
		return 0;
	} 
	return 1;
}

uint8_t WS2812P16::getPixelColorId(uint16_t index) {
	
	if(index < count_led) {
        uint8_t nibble[2];
        uint16_t byteIDx = index >> 1; // Index / 2
        nibble[0] = pixels[byteIDx] & 0xF; // low nibble
        nibble[1] = pixels[byteIDx] >> 4;  // high nibble

        if(index%2 == 0) {    // index is an even number -> Nibble[0]
            return(nibble[0]);
        } else {   // index is an odd number -> Nibble[1]
            return(nibble[1]);
        }
	} 
	return 0xF;
}


void WS2812P16::sync() {
	*ws2812_port_reg |= pinMask; // Enable DDR
//	ws2812_sendarray_mask(pixels,3*count_led,pinMask,(uint8_t*) ws2812_port,(uint8_t*) ws2812_port_reg );	
	ws2812_sendarray_mask(pixels,count_led,pinMask,(uint8_t*) ws2812_port,(uint8_t*) ws2812_port_reg );	
}


WS2812P16::~WS2812P16() {
	free(pixels);
	
}

#ifndef ARDUINO
void WS2812P16::setOutput(const volatile uint8_t* port, volatile uint8_t* reg, uint8_t pin) {
	pinMask = (1<<pin);
	ws2812_port = port;
	ws2812_port_reg = reg;
}
#else
void WS2812P16::setOutput(uint8_t pin) {
	pinMask = digitalPinToBitMask(pin);
	ws2812_port = portOutputRegister(digitalPinToPort(pin));
	ws2812_port_reg = portModeRegister(digitalPinToPort(pin));
}
#endif 
